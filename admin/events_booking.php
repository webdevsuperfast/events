<?php include('header.php');
$fileclass = fileclass2();

if(isset($_GET['approve'])){


  // update function usage
  $array = array(
  	'status'=> 'approved'
  );
  if(update($array,$_GET['approve'],'tbl_book')){
  	?><script>alert('approved');</script><?php
  }else{
  	echo 'error on update';
  }

}
if(isset($_GET['decline'])){
   // update function usage
 $array = array(
  'status'=> 'reject'
);
if(update($array,$_GET['decline'],'tbl_book')){
  ?><script>alert('rejected');</script><?php
}else{
  echo 'error on update';
}
}

?>

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Blank Page</li>
      </ol>
      <div class="row">
        <div class="col-12">
         <h2>List of newly booked</h2>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Event</th>
                  <th>Address</th>
                  <th>Price</th><th>start</th>
                  <th>end</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                 <th>Name</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Event</th>
                  <th>Address</th>
                  <th>Price</th><th>start</th>
                  <th>end</th>
                  <th>Options</th>
                  
                </tr>
              </tfoot>
              <tbody>
              <?php 
        // GET where id 
        $data = custom_query("SELECT * FROM `tbl_book` WHERE `status` is null or `status`=''");
        foreach ($data as $row) {
          ?>
          
            <tr>
              <td><?php echo $row['name'];?></td>
              <td><?php echo $row['email'];?></td>
              <td><?php echo $row['contact'];?></td>
              <td><?php echo $row['event'];?></td>
              <td><?php echo $row['address'];?></td>
              <td><?php echo $row['price'];?></td>
              <td><?php echo $row['start'];?></td>
              <td><?php echo $row['end'];?></td>
              <td>
              <a href="events_booking.php?approve=<?php echo $row['id'];?>" class="btn-info btn">Approve</a>
              <a href="events_booking.php?decline=<?php echo $row['id'];?>" class="btn-danger btn">Reject</a>

              </td>
            </tr>
          
          <?php
        }
        ?>
            </tbody>
</table>
</div>
         
        </div>


        <div class="col-md-12">

        <h2>List of Approved</h2>
        
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Event</th>
                  <th>Address</th>
                  <th>Price</th>
                  <th>start</th>
                  <th>end</th>
                  <!-- <th>Options</th> -->
                </tr>
              </thead>
              <tfoot>
                <tr>
                 <th>Name</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Event</th>
                  <th>Address</th>
                  <th>Price</th>
                  <th>start</th>
                  <th>end</th>
                  <!-- <th>Options</th> -->
                </tr>
              </tfoot>
              <tbody>
              <?php 
        // GET where id 
        $data = custom_query("SELECT * FROM `tbl_book` WHERE `status`='approved'");
        foreach ($data as $row) {
          ?>
          
            <tr>
              <td><?php echo $row['name'];?></td>
              <td><?php echo $row['email'];?></td>
              <td><?php echo $row['contact'];?></td>
              <td><?php echo $row['event'];?></td>
              <td><?php echo $row['address'];?></td>
              <td><?php echo $row['price'];?></td>
              <td><?php echo $row['start'];?></td>
              <td><?php echo $row['end'];?></td>
              <!-- <td>
              <a href="events_booking.php?approve=<?php echo $row['id'];?>" class="btn-info btn">Approve</a>
              <a href="events_booking.php?decline=<?php echo $row['id'];?>" class="btn-danger btn">Reject</a>

              </td> -->
            </tr>
          
          <?php
        }
        ?>
            </tbody>
</table>
</div>
        </div>
        

        <div class="col-md-12">

        <h2>List of Rejected</h2>
       
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Event</th>
                  <th>Address</th>
                  <th>Price</th>
                  <th>start</th>
                  <th>end</th>
                  <!-- <th>Options</th> -->
                </tr>
              </thead>
              <tfoot>
                <tr>
                 <th>Name</th>
                  <th>Email</th>
                  <th>Contact</th>
                  <th>Event</th>
                  <th>Address</th>
                  <th>Price</th>

                  <th>start</th>
                  <th>end</th>
                  <!-- <th>Options</th> -->
                </tr>
              </tfoot>
              <tbody>
              <?php 
        // GET where id 
        $data = custom_query("SELECT * FROM `tbl_book` WHERE `status`='reject'");
        foreach ($data as $row) {
          ?>
          
            <tr>
              <td><?php echo $row['name'];?></td>
              <td><?php echo $row['email'];?></td>
              <td><?php echo $row['contact'];?></td>
              <td><?php echo $row['event'];?></td>
              <td><?php echo $row['address'];?></td>
              <td><?php echo $row['price'];?></td>
              <td><?php echo $row['start'];?></td>
              <td><?php echo $row['end'];?></td>
              <!-- <td>
              <a href="events_booking.php?approve=<?php echo $row['id'];?>" class="btn-info btn">Approve</a>
              <a href="events_booking.php?decline=<?php echo $row['id'];?>" class="btn-danger btn">Reject</a>

              </td> -->
            </tr>
          
          <?php
        }
        ?>
            </tbody>
</table>
</div>
        </div>


        



      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

<?php include('footer.php');?>
</body>

</html>
