<?php include('header.php');
$fileclass = fileclass2();

?>


<?php 

if($_SERVER['REQUEST_METHOD']=="POST"){

  if(isset($_POST['create'])){
        $fileName = $_FILES['image']['name'];
        $tmpName = $_FILES['image']['tmp_name'];
        $fileSize = $_FILES['image']['size'];
        $fileType = $_FILES['image']['type'];
        $filePath =  $upload_dir . $fileName;
        $filePath2 =  $upload_dir2 . $fileName;
        $result = move_uploaded_file($tmpName, $filePath);
        if($result){
          $array = array(
            'title' => $_POST['name'],
            
            'content'=> $_POST['desc'],
            'feat_img' => $filePath2
          );

          if(insert($array,'tbl_page')){
            ?>
            <script>alert('Success');</script>
            <?php
            header('refresh 2s;url=events.php');
          }else{
            header('location:error.php');
          }

        }
  //end create
  }



  if(isset($_POST['update'])){
    $fileName = $_FILES['image']['name'];
    $tmpName = $_FILES['image']['tmp_name'];
    $fileSize = $_FILES['image']['size'];
    $fileType = $_FILES['image']['type'];
    $filePath =  $upload_dir . $fileName;
    $filePath2 =  $upload_dir2 . $fileName;
    $result = move_uploaded_file($tmpName, $filePath);
    if($result){
      $array = array(
        'title' => $_POST['name'],
     
        'content'=> $_POST['desc'],
        'feat_img' => $filePath2
      );

      if(update($array,$_POST['updateid'],'tbl_page')){
        ?>
        <script>alert('Success Update');</script>
        <?php
        header('refresh 2s;url=events.php');
      }else{
        header('location:error.php');
      }

    }
//end create
}



}

?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <!-- <li class="breadcrumb-item active">Blank Page</li> -->
      </ol>
      <div class="row">
      <!-- <div class="col-md-12"><a href="?create=1" class="btn-lg btn btn-primary">Create Page</a></div> -->
      </div> <br><br>
     
        
         
       

        
        <?php 
        if(isset($_GET['create'])){
          //start create
          ?>
          <form action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" method = "post">
          <input type="hidden" name="create" value="create" >
          
          <label for="title">Fill up form</label><br>
          
          <label for="title">Page title</label><br>
          <input type="text" name="name" required><br>


          


          <label for="content">Page Content</label>
          <textarea name="desc" id="" cols="30" rows="10" class="form-control textarea " ></textarea>

            
            <input id="avatar-1" name="image" type="file" class="file-loading" required>
            <br><br>
            <button type="submit" value="submit" class="btn btn-primary btn-lg">Create</button>
            </form>
          <?php

          //end create
        }elseif(isset($_GET['updateid'])){
          //start update
          $updateid = $_GET['updateid'];

          // GET where id 
          $data = get_whereid('tbl_page' , $updateid);
          foreach ($data as $row) {
             if(isset($row)){
                // echo "update set";
                ?>

<form action="<?php echo $_SERVER['PHP_SELF'];?>" enctype="multipart/form-data" method = "post">
          <input type="hidden" name="update" value="update" >
          <input type="hidden" name="updateid" value="<?php echo $row['id'];?>" >
          <label for="title">Fill up form</label><br>
          
          <label for="title">Page Title</label><br>
          <input type="text"  value="<?php echo $row['title'];?>" disabled required><br>
          <input type="hidden" name="name" value="<?php echo $row['title'];?>"  ><br>
  
          
        

          <label>Page Content</label><br>
          <textarea name="desc" id="" cols="30" rows="10" class="form-control textarea " ><?php echo $row['content'];?></textarea>

            
            <input id="avatar-1" name="image" type="file" class="file-loading" required>
            <br><br>
            <button type="submit" value="submit" class="btn btn-primary btn-lg">Update</button>
            </form>
                <?php

             }else{
                header('location:error.php');
             }
          }
          //end update
        }else{
          // data events display
          $data = get('tbl_page');
          foreach ($data as $row) {
             ?>
            <div class="row">
                
                  <div class="col-md-4"><img src="../<?php echo $row['feat_img'];?>" alt="" class="img img-responsive img-fluid"><br>
                  <a href="?updateid=<?php echo $row['id'];?>" class="btn btn-warning btn-lg"> Update </a>
                  <!-- <a href="delete.php?id=<?php echo $row['id'];?>&file=<?php echo $fileclass;?>&table=tbl_page" class="btn btn-danger btn-lg"> Delete </a> -->
                  </div>
                  <div class="col-md-8">
                    <h2><?php echo $row['title'] ?></h2>
                    <!-- <h3>package price <?php echo $row['price'];?></h3> -->
                    <section class="description"><?php echo $row['content'];?></section>
                  </div>
            
            </div>
            <br>
             <?php 
          }
          //end  else
        }
        ?>




         
        
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

<?php include('footer.php');?>
</body>

</html>
