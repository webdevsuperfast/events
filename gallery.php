<?php include('functions.php'); ?>
<?php include($partials . 'header.php'); ?>
<?php
// uncomment for session auto start
// session_starter();
?>

<body class="<?php fileclass(); ?>">
  <?php include($partials . 'menu.php'); ?>
  <link rel="stylesheet" href="assets/css/fancy.css">
  <!-- Page Content -->
  <div class="container page-top">
    <div class="row">

      <?php
      // GET usage
      $data = get('tbl_gallery');
      foreach ($data as $row) {
      ?>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
          <a href="<?php echo $row['feat_img']; ?>" class="fancybox" rel="ligthbox">
            <img src="<?php echo $row['feat_img']; ?>" class="zoom img-fluid " alt="">

          </a>
        </div>
      <?php
      }

      ?>
    </div>
  </div>
  <?php include($partials . 'footer.php'); ?>