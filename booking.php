<?php include('functions.php'); ?>
<?php include($partials . 'header.php'); ?>
<?php
// uncomment for session auto start
session_starter();

if ($_SERVER['REQUEST_METHOD'] == "POST") {
  if (isset($_POST['id_book'])) {
    $array = array(
      'name' => $_POST['name'],
      'email' => $_POST['email'],
      'contact' => $_POST['contact'],
      'address' => $_POST['address'],
      'event' => $_POST['event'],
      'price' => $_POST['price'],
      'start' => $_POST['start'],
      'end' => $_POST['end']

    );
    if (insert($array, 'tbl_book')) {
?>
      <script>
        alert('Booked!');
      </script>
<?php
      header('refresh:1;url=./');
    } else {
      echo 'error on insert';
    }
  }
}


?>

<body class="<?php fileclass(); ?>">
  <?php include($partials . 'menu.php'); ?>


  <?php if ($_SERVER['REQUEST_METHOD'] == "POST") {

    if (isset($_POST['id'])) {
  ?>
      <div class="bookingform">
        <div class="container">
          <div class="row">
            <div class="col-md-12">


              <form action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data" method="post">

                <input type="hidden" name="event" value="<?php echo $_POST['event']; ?>">
                <input type="hidden" name="id_book" value="<?php echo $_POST['id']; ?>">

                <label for="title">Fill up form</label><br>

                <label for="title">Name</label><br>
                <input type="text" name="name" required><br>
                <label for="title">email</label><br>
                <input type="email" name="email" required><br>
                <label for="title">contact</label><br>
                <input type="test" name="contact" required><br>


                <label for="title">start</label><br>
                <input type="date" name="start" id="start" class="date" min="<?php echo date("Y-m-d"); ?>" required><br>

                <label for="title">end</label><br>
                <input type="date" name="end" id="end" class="date" min="<?php echo date("Y-m-d"); ?>" required><br>
                <label for="" id="calculate"></label>
                <label for="">Package Price | <?php echo $_POST['days']; ?> Days</label>

                <input type="text" name="pricex" value="<?php echo $_POST['price']; ?>" id="pricex" disabled>
                <br>
                <label for="">Price Per Day</label>
                <input type="hidden" value="<?php echo $_POST['days']; ?>" id="days">
                <input type="text" name="pricex" value="<?php echo $_POST['price_day']; ?>" id="priced" disabled>
                <br>
                <span id="note">

                </span>


                <label for="title">address</label><br>
                <textarea name="address" id="textadd" cols="30" rows="10"></textarea>



                <br><br>
                <button type="submit" value="submit" class="btn btn-primary btn-lg">Book</button>
              </form>

            </div>
          </div>
        </div>

      </div>
  <?php
    }
  }

  ?>


  <?php include($partials . 'footer.php'); ?>