<?php include('functions.php'); ?>
<?php include($partials . 'header.php'); ?>
<?php
// uncomment for session auto start
// session_starter();
?>

<body class="<?php fileclass(); ?>">
  <?php include($partials . 'menu.php'); ?>

  <?php
  // get where field
  $data = custom_query("select * from tbl_page where title='About Us'");
  foreach ($data as $row) {

    $xx = $row['id'];
    if ($xx != "") {
  ?>
      <div class="banner" style="background-image:url(<?php echo $row['feat_img']; ?>);">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="h1"><?php echo $row['title']; ?></div>
            </div>

          </div>

        </div>
      </div>


      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <?php echo $row['content']; ?>
          </div>
        </div>
      </div>
    <?php
    } else {
    ?>


      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="h1">Page doesnt Exist</div>
          </div>
        </div>
      </div>

  <?php
    }
  }

  ?>


  <?php include($partials . 'footer.php'); ?>