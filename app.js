$(document).ready(function() {
  //scripts here
  $("input , #textadd").addClass("form-control");

  $(".fancybox").fancybox({
    openEffect: "none",
    closeEffect: "none"
  });

  $(".zoom").hover(
    function() {
      $(this).addClass("transition");
    },
    function() {
      $(this).removeClass("transition");
    }
  );
  //emd ready

  $("#end").on("change", function() {
    var date1 = new Date(document.getElementById("start").value);
    var date2 = new Date(document.getElementById("end").value);
    var totalx = "";
    var calc = "";
    var difference = date2 - date1;

    var days = difference / (24 * 3600 * 1000);
    // alert(days);

    $("#calculate").html(
      "You have selected " +
        days +
        ' days <input type="hidden" name="days" value="' +
        days +
        '">'
    );
    var price_p = $("#pricex").val();
    var price_d = $("#priced").val();
    var days_p = $("#days").val();
    // console.log(price_p);

    if (days <= days_p) {
      totalx = price_p;
    } else {
      var datex = parseFloat(days) - parseFloat(days_p);
      calc = datex * price_d;
      totalx = calc + parseFloat(price_p);

      console.log("overlap " + datex);
      console.log(calc);
      console.log(parseInt(price_p));
      console.log(totalx);
    }

    $("#note").html(
      "Total Price : " +
        totalx +
        '<input type="hidden" name="price" value="' +
        totalx +
        '" id="finalprice"  > | ' +
        datex +
        " days Overlapped <br>"
    );
  });
});
