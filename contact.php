<?php include('functions.php'); ?>
<?php include($partials . 'header.php'); ?>
<?php
// uncomment for session auto start
// session_starter();
?>

<body class="<?php fileclass(); ?>">
  <?php include($partials . 'menu.php'); ?>



  <!-- Contact Us Section -->
  <section class="Material-contact-section section-padding section-dark">
    <div class="container">
      <div class="row">
        <!-- Section Titile -->
        <div class="col-md-12 wow animated fadeInLeft" data-wow-delay=".2s">
          <h1 class="section-title">Edwin Vitos Caballero Jr.</h1>
        </div>
      </div>
      <div class="row">
        <!-- Section Titile -->
        <div class="col-md-6 mt-3 contact-widget-section2 wow animated fadeInLeft" data-wow-delay=".2s">
          <p><span>Thank You For Trusting Edzky-Ian Styling Event</span></p>

          <div class="find-widget">
            Email Address: <a href="#">craig_edzky31@yahoo.com</a>
          </div>
          <div class="find-widget">
            Address: <a href="#">Sto.Domingo Piat, Cagayan</a>
          </div>
          <div class="find-widget">
            Phone: <a href="#">+ 639366171956/09394989472</a>
          </div>

          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d282.44378173131105!2d121.50574423889088!3d17.740142725746377!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x338f64f64e5001a1%3A0xb207e383b38bcce0!2sSto.%20Domingo%20Chapel!5e0!3m2!1sen!2sph!4v1576686023982!5m2!1sen!2sph" width="550" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

        </div>
        <!-- contact form -->
        <div class="col-md-6 wow animated fadeInRight" data-wow-delay=".2s">
          <form class="shake" role="form" method="post" id="contactForm" name="contact-form" data-toggle="validator">
            <!-- Name -->
            <div class="form-group label-floating">
              <label class="control-label" for="name">Name</label>
              <input class="form-control" id="name" type="text" name="name" required data-error="Please enter your name">
              <div class="help-block with-errors"></div>
            </div>
            <!-- email -->
            <div class="form-group label-floating">
              <label class="control-label" for="email">Email</label>
              <input class="form-control" id="email" type="email" name="email" required data-error="Please enter your Email">
              <div class="help-block with-errors"></div>
            </div>
            <!-- Subject -->
            <div class="form-group label-floating">
              <label class="control-label">Subject</label>
              <input class="form-control" id="msg_subject" type="text" name="subject" required data-error="Please enter your message subject">
              <div class="help-block with-errors"></div>
            </div>
            <!-- Message -->
            <div class="form-group label-floating">
              <label for="message" class="control-label">Message</label>
              <textarea class="form-control" rows="3" id="message" name="message" required data-error="Write your message"></textarea>
              <div class="help-block with-errors"></div>
            </div>
            <!-- Form Submit -->
            <div class="form-submit mt-5">
              <button class="btn btn-common" type="submit" id="form-submit"><i class="material-icons mdi mdi-message-outline"></i> Send Message</button>
              <div id="msgSubmit" class="h3 text-center hidden"></div>
              <div class="clearfix"></div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>



  <?php include($partials . 'footer.php'); ?>