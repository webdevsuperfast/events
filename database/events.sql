-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2019 at 09:13 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `events`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_book`
--

CREATE TABLE `tbl_book` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `contact` text NOT NULL,
  `event` text NOT NULL,
  `price` text NOT NULL,
  `status` text NOT NULL,
  `notif` text NOT NULL,
  `address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_book`
--

INSERT INTO `tbl_book` (`id`, `name`, `email`, `contact`, `event`, `price`, `status`, `notif`, `address`) VALUES
(10, 'test', 'x@x.com', '123', 'Wedding', '90000', 'reject', '', 'San Gabriel'),
(4, 'test', 'test@test.com', '123', 'test\'s test', '1231', 'reject', '', 'test'),
(6, 'teeeeest', 'test@test.com', '123', 'barong', '400', 'reject', '', 'San Gabriel'),
(7, 'kamille', 'camillejoy0027@gmail.com', '09555023203', 'Wedding', '', 'approved', '', '12eefew'),
(8, 'krizza', 'krizza@email.com', '09653027053', 'Wedding', '60000', 'approved', '', ''),
(9, 'dasdsada', 'dsad@gmail.com', 'sad', 'Wedding', '240000', 'reject', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events`
--

CREATE TABLE `tbl_events` (
  `id` int(11) NOT NULL,
  `event` text NOT NULL,
  `price` text NOT NULL,
  `description` text NOT NULL,
  `feat_img` text NOT NULL,
  `price_day` text NOT NULL,
  `days` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_events`
--

INSERT INTO `tbl_events` (`id`, `event`, `price`, `description`, `feat_img`, `price_day`, `days`) VALUES
(19, 'Wedding', '30000', '<ul>\r\n<li>Simple Styling with decorated Gazebo for the couple.</li>\r\n<li>Fresh flower centerpis (couple table only)</li>\r\n<li>Gown Set (limited only)\r\n<ul>\r\n<li style=\"list-style-type: none;\">\r\n<ul>\r\n<li>Couple Suit, 2 Parents Suit, Maid of Honor, Bestman, 3 Bridesmaids, 3 Groom\'s Men(barong), 3 Bearers(barong), 3 Flower Girl\'s with decorated baskets.&nbsp;</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</li>\r\n<li>30 pcs. Cebu Souvenirs</li>\r\n<li>30 pcs. Tube Invitations</li>\r\n<li>2x3 Tarpaulin</li>\r\n<li>Boquet (Bride Only)</li>\r\n<li>Cake (Fondant Icing)</li>\r\n<li>A bottle of wine for ceremonial toast</li>\r\n<li>A pair of dove with decorated cage (rent dove only)</li>\r\n<li>Hair &amp; Make-Up (free)</li>\r\n<li>50 pcs. Corsages (ribbon only)</li>\r\n<li>Simple Bridal Car Decoration</li>\r\n</ul>', 'assets/images/79679100_2664983570245664_2723462839980261376_o.jpg', '5000', '2'),
(20, 'Birthday', '30000', '', 'assets/images/51410422_2112873542123339_1628339822672216064_n.jpg', '5000', '2'),
(21, 'Christening', '20000', '', 'assets/images/42350817_1926683554075673_392461424175087616_n.jpg', '5000', '2'),
(22, 'test', '1000', '<p>test</p>', 'assets/images/food-platter-2175326_1920.png', '500', '2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `id` int(11) NOT NULL,
  `feat_img` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `feat_img`) VALUES
(6, 'assets/images/65839737_2350418311702193_263121915734917120_n.jpg'),
(7, 'assets/images/54518936_2182001935210499_3253280380593111040_n.jpg'),
(8, 'assets/images/FB_IMG_1576511419263[1].jpg'),
(9, 'assets/images/FB_IMG_1576511428580[1].jpg'),
(10, 'assets/images/68241434_2425632217514135_7740697537559920640_n.jpg'),
(12, 'assets/images/51410422_2112873542123339_1628339822672216064_n.jpg'),
(13, 'assets/images/FB_IMG_1576513234182[1].jpg'),
(14, 'assets/images/FB_IMG_1576513143707[1].jpg'),
(15, 'assets/images/FB_IMG_1576513132860[1].jpg'),
(16, 'assets/images/FB_IMG_1576513081514[1].jpg'),
(17, 'assets/images/FB_IMG_1576513074058[1].jpg'),
(18, 'assets/images/FB_IMG_1576513409384[1].jpg'),
(19, 'assets/images/FB_IMG_1576513333140[1].jpg'),
(20, 'assets/images/FB_IMG_1576513412040[1].jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_page`
--

CREATE TABLE `tbl_page` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `feat_img` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_page`
--

INSERT INTO `tbl_page` (`id`, `title`, `content`, `feat_img`) VALUES
(1, 'About Us', '<h1><strong>Mission:</strong></h1>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; To make an amazing, fantastic, and unforgetable event. To serve the best to the client with complete dedication and create the unique ideal for entire event.</p>\r\n<p>&nbsp;</p>\r\n<h1><strong>Vision: </strong></h1>\r\n<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>To provide edzky-ian styling event the perfect choice for big events. As every event has to be perfect and creative. Ensures client satisfactory and loyalty.</p>\r\n<p>&nbsp;</p>', 'assets/images/68241434_2425632217514135_7740697537559920640_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `access` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `access`) VALUES
(1, 'admin', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_book`
--
ALTER TABLE `tbl_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_events`
--
ALTER TABLE `tbl_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_page`
--
ALTER TABLE `tbl_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_book`
--
ALTER TABLE `tbl_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_events`
--
ALTER TABLE `tbl_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_page`
--
ALTER TABLE `tbl_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
