<?php include('functions.php'); ?>
<?php include($partials . 'header.php'); ?>
<?php
// uncomment for session auto start
// session_starter();
?>

<body class="<?php fileclass(); ?>">
  <?php include($partials . 'menu.php'); ?>




  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <h1>Events Services</h1>
        <br><br>
      </div>
    </div>



    <?php

    // GET usage
    $data = get('tbl_events');
    foreach ($data as $row) {
    ?>
      <div class="row">
        <div class="col-md-4">
          <img src="<?php echo $row['feat_img']; ?>" alt="" class="img img-fluid">
        </div>
        <div class="col-md-8">
          <h2><?php echo $row['event'] ?></h2>
          <h3>package price <?php echo $row['price']; ?></h3>
          <section class="description"><?php echo $row['description']; ?></section>
          <form action="booking.php" method="post">
            <input type="hidden" name="event" value="<?php echo $row['event']; ?>">
            <input type="hidden" name="price_day" value="<?php echo $row['price_day']; ?>">
            <input type="hidden" name="days" value="<?php echo $row['days']; ?>">
            <input type="hidden" name="price" value="<?php echo $row['price']; ?>">
            <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
            <input type="submit" value="Book" class="btn btn-lg btn-info">
          </form>
        </div>
      </div>
      <hr />
    <?php
    }

    ?>

  </div>


  <?php include($partials . 'footer.php'); ?>