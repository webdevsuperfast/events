<?php include('functions.php'); ?>
<?php include($partials . 'header.php'); ?>
<?php
// uncomment for session auto start
// session_starter();
?>

<body class="<?php fileclass(); ?>">
  <?php include($partials . 'menu.php'); ?>


  <div class="container">
    <div class="row blur">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <h1>YOUR PERFECT CHOICE FOR YOUR BIG EVENTS</h1>
      </div>
      <div class="col-md-2"></div>
    </div>
  </div>


  <?php include($partials . 'footer.php'); ?>